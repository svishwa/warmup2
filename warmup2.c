#include <stdio.h>
#include <stdlib.h>
#include "cs402.h"
#include "my402list.h"
#include <pthread.h>
#include <signal.h>
#include <string.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <math.h>
#include <unistd.h>

double pstime=0,res=0,avg_Q1_pkt=0,avg_Q2_pkt=0,avg_S1_pkt=0,avg_S2_pkt=0,avg_time_sys=0,left_SD=0,right_SD=0;
int numS=0,server=0,pnum=0;
FILE* fp=0L;
double tetime,tstime=0;
double prevptime=0;
int mode=0,tokenCnt=0,tokendrop=0,totalT=0;
int totalPackets=0,packetdrop=0;
struct timeval start;
pthread_mutex_t mutex=PTHREAD_MUTEX_INITIALIZER;
sigset_t set;
My402List *Q1List,*Q2List,*token_bucket_list;
pthread_t packet_thread,token_thread,server_thread_1,server_thread_2,signal_thread;
int num=0,B=0,R=0;
pthread_cond_t cv;
float avg_inter=0,avg_inter_div=0;
float avg_ser=0,avg_ser_div=0;

typedef struct layout
{
	float mu,lambda,r;
	long B,P,num;
	char* fileCheck;
}info;

typedef struct  packet
{
	float lambda,packet_arrival_time;
	float start_timeQ1,end_timeQ1,start_timeQ2,end_timeQ2,start_timeServer,end_timeServer;
	int req_tokens;
	float mu;
	int packet_num;
}packetStr;

typedef struct serverstr
{
	int num;
	int busy;
	//packetStr* list;
}serinfo;

void prepare_packet(packetStr* packet,int pCnt,info* str);
void doAppend(packetStr*);

double tconvert(struct timeval cal)
{
	double res;
	res=1000000*((double)cal.tv_sec-(double)start.tv_sec)+((double)cal.tv_usec-(double)start.tv_usec);
	return res/1000;
}

//checks for validity of input given to each option
void error(long val,char* option)
{
//	printf("here\n");
	if(val>2147483647)
	{
		fprintf(stderr, "%s %s\n", option,"exceeds maximum value" );
		exit(1);
	}
}
//input given is within the approved condition

//checks for validity of input given to each option
void checkForNegative(char* input,char* option)
{
	if(input[0]=='-')
	{
		fprintf(stderr, "%s %s\n", option,"has a negative value as input");
		exit(1);
	}
}
//input given is within the approved condition

//Process command line arguments
void process(info *str,int argc,char *argv[])
{
	int i=0;
	for(i=1;i<argc;i+=2)
	{
		if(strcmp(argv[i],"-lambda")==0)
		{
			str->lambda=(double)1/atof(argv[i+1]);
			if(mode==0)
			{
				if(str->lambda>10)
					str->lambda=10;
				str->lambda*=1000;
			}
			checkForNegative(argv[i+1],argv[i]);
			//continue;
		}
		else if(strcmp(argv[i],"-mu")==0)
		{
			str->mu=(double)1/atof(argv[i+1]);
			if(mode==0)
			{
				if(str->mu>10)
					str->mu=10;
				str->mu*=1000;
			}
			checkForNegative(argv[i+1],argv[i]);
			//continue;
		}
		else if(strcmp(argv[i],"-r")==0)
		{
			str->r=(double)1/atof(argv[i+1]);
			if(str->r>10)
				str->r=10;
			str->r*=1000;
			checkForNegative(argv[i+1],argv[i]);
			//continue;
		}
		else if(strcmp(argv[i],"-B")==0)
		{
			str->B=atol(argv[i+1]);
			error(str->B,argv[i]);
			checkForNegative(argv[i+1],argv[i]);
			//continue;
		}
		else if(strcmp(argv[i],"-P")==0)
		{
			str->P=atol(argv[i+1]);
			error(str->P,argv[i]);
			checkForNegative(argv[i+1],argv[i]);
			//continue;
		}
		else if(strcmp(argv[i],"-n")==0)
		{
			str->num=atol(argv[i+1]);
			//printf("here");
			error(str->num,argv[i]);
			checkForNegative(argv[i+1],argv[i]);
			//continue;
		}
		else if(strcmp(argv[i],"-t")==0)
		{
			str->fileCheck=malloc(sizeof(char)*strlen(argv[i+1]));
			strcpy(str->fileCheck,argv[i+1]);
			//continue;
		}
		else
		{
			fprintf(stderr,"%s","Please enter correct option\n");
			exit(1);
		}

	}
}
//correct values assigned to the structure 

void append(char* val,char tmp)
{
	int len=strlen(val);
	char* buf=malloc(len+2);
	strcpy(buf,val);
	buf[len]=tmp;
	buf[len+1]='\0';
	strcpy(val,buf);
}

void processLine(char *line,info *str,int lineCnt)
{
	//printf("%s\n","reached" );
	int valCnt=0;
	if(lineCnt==0)
		{
			str->num=atol(line);
			num=str->num;
			totalPackets=num;
		}
	else
	{
		info *strt=malloc(sizeof(info));
		//I wrote this code in warmup1 and borrowing it here
		int j=0;
		char* val=malloc(1024*sizeof(char));
		int flag=0;
		for(j=0;j<strlen(line);j++)
		{
			if(line[j]!='\t' && line[j]!=' ')
			{
				append(val,line[j]);
				flag=1;
			}
			else
			{
				//printf("%s\n",val);
				if(valCnt==0 && flag==1)
					str->lambda=atof(val);
				else if(valCnt==1 && flag==1)
					str->P=atol(val);
				else if(valCnt==2 && flag==1)
					{
						str->mu=atof(val);
					}
				else if(valCnt>2)
				{
					fprintf(stderr, "%s %d %s\n", "Invalid format of the line at",lineCnt," number");
					exit(1);
				}
				memset(val,0,sizeof(char)*1024);
				if(flag==1)
					valCnt++;
				flag=0;
			}
		}
		if(valCnt==2 && flag==1)
			{
				str->mu=atof(val);
			}
		strt->B=str->B;
		strt->r=str->r;
		strt->num=str->num;
		strt->mu=str->mu;
		strt->lambda=str->lambda;
		strt->P=str->P;
		//printf("str mu: %lf\n",strt->mu );
		//printf("str lambda:%lf\n",strt->lambda);
		//printf("str p: %d\n",str->P );
		//fprintf(stdout, "%d\n",strt->P );
		packetStr* packet=malloc(sizeof(packetStr));
		prepare_packet(packet,lineCnt,strt);
		//printf("packet->lamda:%f\n",packet->lambda );
		//printf("pack req_tokens:%d\n",packet->req_tokens );
		//printf("packet mu:%f\n",packet->mu );
		//fprintf(stdout, "packet number req : %d\t bucket size : %d\n",packet->req_tokens,B );
		double arrtime;
		double sleeptime,interval;
		struct timeval endt;
		interval=packet->lambda;
		pthread_mutex_unlock(&mutex);
		gettimeofday(&endt,0L);
		sleeptime=interval-(tconvert(endt)-pstime);
		if(sleeptime<0)
			usleep(0);
		else
			usleep(sleeptime*1000);
		if(packet->req_tokens>B)
		{
			packetdrop++;
			totalPackets--;
			gettimeofday(&endt,0L);
			fprintf(stdout,"%012.3fms: p%d arrives, needs %d tokens, inter-arrival time = %0.3f, dropped\n",tconvert(endt),packet->packet_num,packet->req_tokens,tconvert(endt)-pstime);
			avg_inter+=(tconvert(endt)-pstime);
			avg_inter_div++;
			pstime=tconvert(endt);
			free(packet);
			pthread_mutex_unlock(&mutex);
			return;
		}
		gettimeofday(&endt,0L);
		arrtime=tconvert(endt);
		
		pthread_mutex_lock(&mutex);
		doAppend(packet);
		fprintf(stdout, "%012.3fms: p%d arrives,needs %d %s%.3fms\n",arrtime,packet->packet_num,packet->req_tokens,"tokens, inter-arrival time = ",arrtime-pstime );
		avg_inter+=(tconvert(endt)-pstime);
		avg_inter_div++;
		pstime=arrtime;
		fprintf(stdout, "%012.3fms: p%d enters Q1\n",packet->start_timeQ1,packet->packet_num );
		totalPackets--;
		prevptime=packet->start_timeQ1;
		pthread_mutex_unlock(&mutex);
	}
}

void readFile(FILE *fp,info * str)
{
	/*I wrote this code in warmup1 and borrowing it here*/
	const int lineSize=1200;
	char line[lineSize];
	int lineCnt=0;
	while(fgets(line,lineSize,fp)!=0L)
	{
	//	printf("In packet\n");
		if(strlen(line)>1024)
		{
			fprintf(stderr,"%s","line exceeds 1024 characters\n" );
			exit(1);
		}
		processLine(line,str,lineCnt);
		lineCnt++;
	}
	fclose(fp);
}

void print(info *str)
{
	printf("%s\n","Emulation parameters:" );
	printf("    %s = %ld\n","number to arrive",str->num );
	if(mode==0)
	{
		printf("    %s = %.2f\n","lambda",1000/str->lambda );
		printf("    %s = %.2f\n","mu",1000/str->mu );
	}
	printf("    %s = %.2f\n","r",1000/str->r );
	printf("    %s = %ld\n","B",str->B );
	if(mode==0)
		printf("    %s = %ld\n","P",str->P);
	if(mode==1)
		printf("    %s = %s\n","tsfile",str->fileCheck );
}

void doAppend(packetStr* p)
{
	struct timeval appendtime;
	My402ListAppend(Q1List,(void*)p);
	gettimeofday(&appendtime,0L);
	p->start_timeQ1=tconvert(appendtime);
	//fprintf(stdout, "%012.3fms: p%d arrives,needs %d %s%.3fms\n",(p->start_timeQ1),p->packet_num,p->req_tokens,"tokens, inter-arrival time = ",p->start_timeQ1-prevptime );
}

void move_to_Q2_Q1()
{
	struct timeval endQ1,startQ2;
	My402ListElem *elem=My402ListFirst(Q1List);
	if(elem!=0L)
	{
		packetStr* p=(packetStr*)elem->obj;
		if( p->req_tokens<=tokenCnt)
		{
			tokenCnt-=p->req_tokens;
			My402ListUnlink(Q1List,elem);
			gettimeofday(&endQ1,0L);
			p->end_timeQ1=tconvert(endQ1);
			avg_Q1_pkt+=(p->end_timeQ1-p->start_timeQ1);
			fprintf(stdout, "%012.3fms: %s%d %s, %s%.3fms%s%d%s\n",(p->end_timeQ1),"packet p",
				p->packet_num,"leaves Q1","time in Q1 = ",-(p->start_timeQ1)+(p->end_timeQ1),", token bucket now has ",tokenCnt," token" );
			My402ListAppend(Q2List,(void*)p);
			gettimeofday(&startQ2,0L);
			p->start_timeQ2=tconvert(startQ2);
			fprintf(stdout, "%012.3fms: p%d enters Q2\n",(p->start_timeQ2),p->packet_num );
		}
	}
}

void prepare_packet(packetStr* packet,int pCnt,info* str)
{
	packet->lambda=str->lambda;
	packet->req_tokens=str->P;
	packet->packet_num=pCnt;
	packet->mu=str->mu;
}

void handle_packets(info *str)
{
	double sleeptime=0;
	double interval;
	struct timeval endt;
	int pCnt=0;
	printf("%s\n","enter packet thread" );
	if(mode==0)
	{
		while(pCnt<num)
		{
			//printf("In packet\n");
			pCnt++;
			pthread_mutex_lock(&mutex);
			packetStr* packet=malloc(sizeof(packetStr));
			prepare_packet(packet,pCnt,str);
			pthread_mutex_unlock(&mutex);
			double arrtime;
			gettimeofday(&endt,0L);
			interval=packet->lambda;
			struct timeval timearr;
			gettimeofday(&timearr,0L);
			arrtime=tconvert(timearr);
			sleeptime=interval-(tconvert(endt)-pstime);
			if(sleeptime<0)
				usleep(0);
			else
				usleep(sleeptime*1000);
			pthread_mutex_lock(&mutex);
			if(packet->req_tokens>B)
			{
				packetdrop++;
				totalPackets--;
				gettimeofday(&endt,0L);
				fprintf(stdout,"%012.3fms: p%d arrives, needs %d tokens, inter-arrival time = %0.3f, dropped\n",tconvert(endt),packet->packet_num,packet->req_tokens,tconvert(endt)-pstime);
				avg_inter+=(tconvert(endt)-pstime);
				avg_inter_div++;
				pstime=tconvert(endt);
				free(packet);
				if(totalPackets==0)
					break;
			}
			else{
			gettimeofday(&endt,0L);
			fprintf(stdout, "%012.3fms: p%d arrives,needs %d %s%.3fms\n",tconvert(endt),packet->packet_num,packet->req_tokens,"tokens, inter-arrival time = ",arrtime-pstime );
			avg_inter+=(tconvert(endt)-pstime);
			avg_inter_div++;
			pstime=tconvert(endt);
			doAppend(packet);
			fprintf(stdout, "%012.3fms: p%d enters Q1\n",packet->start_timeQ1,packet->packet_num );
			totalPackets--;
			prevptime=packet->start_timeQ1;
			}
		pthread_mutex_unlock(&mutex);
		}
		
		//printf("out of packet loop\n");
	}
	if(mode==1)
	{
		pthread_mutex_lock(&mutex);
		fp=fopen(str->fileCheck,"r");
		readFile(fp,str);
		//printf("out of packet loop\n");
	}
	
}

void handle_tokens(void* arg)
{
	double sleeptime;
	double interval=R;
	struct timeval endt;
	while(1)
	{
		//printf("In token\n");
		pthread_mutex_lock(&mutex);
		if(My402ListLength(Q2List)!=0)
			pthread_cond_broadcast(&cv);
		pthread_mutex_unlock(&mutex);
		gettimeofday(&endt,0L);
		sleeptime=interval-(tconvert(endt)-tstime);
		if(sleeptime<0)
			usleep(0);
		else
			usleep(sleeptime*1000);
		//printf("here : %d\n",totalPackets);
		if(My402ListLength(Q2List)==0 && My402ListLength(Q1List)==0 && totalPackets==0)
		{
			pthread_cond_broadcast(&cv);
			pthread_mutex_unlock(&mutex);
			break;
		}
		totalT++;
		struct timeval end;
		double tarrt;
		gettimeofday(&end,0L);
		tarrt=tconvert(end);
		tstime=tarrt;
		pthread_mutex_lock(&mutex);
		if(tokenCnt<B)
			{
				tokenCnt++;
				fprintf(stdout, "%012.3lfms: %s%d %s %d ",tarrt,"token t",totalT,"arrives, token bucket has now",tokenCnt );
				if(tokenCnt==1)
					fprintf(stdout, "%s\n","token" );
				else
					fprintf(stdout, "%s\n","tokens" );
			}
		else
		{
			tokendrop++;
			if(totalPackets!=0)
			{
				fprintf(stdout,"%012.3fms token arrives,dropped\n",tarrt );
				//pthread_mutex_unlock(&mutex);
				//continue;
			}
			if(My402ListLength(Q2List)==0 && My402ListLength(Q1List)==0 && totalPackets==0)
			{
				pthread_mutex_unlock(&mutex);
				break;
			}
			
		}

		if(My402ListLength(Q1List)!=0)
		{
			move_to_Q2_Q1();
		}
		pthread_mutex_unlock(&mutex);
	}
	

	//printf("out of token loop\n");

}

void handle_server(serinfo* arg)
{
	int i=0,ser;
	My402ListElem* elem;
	packetStr* packet;
	struct timeval Q2end;
	struct timeval serEnd;
	//printf("enter server\n");
	for(i=0;i<num;i++)
	{
		ser=arg->num;
		pthread_mutex_lock(&mutex);
		//pthread_cleanup_push(pthread_mutex_unlock,&mutex);
		//printf("%s\n","before going to gaurd" );
		while(My402ListLength(Q2List)==0 
			&& (My402ListLength(Q1List)>0 || totalPackets>0))
			{
				//printf("%s","in gaurd\n" );
				pthread_cond_wait(&cv,&mutex);
				//printf("q2len : %d\tq1 len : %d\t totalPackets:%d\n",My402ListLength(Q2List),My402ListLength(Q1List),totalPackets);
			}
		//printf("%s\n","after going to gaurd" );
		elem=My402ListFirst(Q2List);
		if(elem==0L && server==1)
		{
			pthread_cond_broadcast(&cv);
		}
		if(arg->busy==0 && elem!=0L)
		{
			packet=(packetStr*)elem->obj;
			numS++;
			My402ListUnlink(Q2List,elem);
			gettimeofday(&Q2end,0L);
			packet->end_timeQ2=tconvert(Q2end);
			fprintf(stdout, "%012.3fms: p%d leaves Q2,time in Q2 = %.3fms\n",packet->end_timeQ2,packet->packet_num,packet->end_timeQ2-packet->start_timeQ2 );
			gettimeofday(&Q2end,0L);
			packet->start_timeServer=tconvert(Q2end);
			avg_Q2_pkt+=(packet->end_timeQ2-packet->start_timeQ2);
			arg->busy=1;
			fprintf(stdout, "%012.3fms: p%d begins service at s%d, requesting %.3fms of service\n",packet->start_timeServer,packet->packet_num,ser,packet->mu );
			pthread_mutex_unlock(&mutex);
			usleep(packet->mu*1000);
			pthread_mutex_lock(&mutex);
			//pthread_cleanup_push(pthread_mutex_unlock,&mutex);
			arg->busy=0;
			gettimeofday(&serEnd,0L);
			packet->end_timeServer=tconvert(serEnd);
			if(arg->num==1)
				avg_S1_pkt+=(packet->end_timeServer-packet->start_timeServer);
			if(arg->num==2)
				avg_S2_pkt+=(packet->end_timeServer-packet->start_timeServer);
			//fprintf(stdout, "start_timeserver:%f\tendtime server:%f\n",packet->start_timeServer,packet->end_timeServer );
			printf( "%012.3fms: p%d departs from s%d, service time = %.3fms, time in system = %.3fms\n",packet->end_timeServer,packet->packet_num,ser,(packet->end_timeServer)-packet->start_timeServer,packet->end_timeServer-packet->start_timeQ1);
			avg_ser+=(-packet->start_timeServer+packet->end_timeServer);
			avg_ser_div++;
			avg_time_sys+=(packet->end_timeServer-packet->start_timeQ1);
			left_SD+=((packet->end_timeServer-packet->start_timeQ1)*(packet->end_timeServer-packet->start_timeQ1));
			free(packet);
			/*printf("%s\n","in if" );
			if(server==1)
			{
				pthread_exit((void*));
			}
			*/
		}
		if(server==1)
		{
			arg->busy=0;
			num=-1;
			pthread_cond_broadcast(&cv);
		}
		pthread_mutex_unlock(&mutex);
	}
}

void print_Stats()
{
	//printf("arr:%d\tser:%d\n",avg_inter,avg_ser );
	//printf("arr div:%d\tser div:%d\n",avg_inter_div,avg_ser_div );
	fprintf(stdout, "%s\n\n","Statistics:" );
	float res1,res2,res3,res4,res5,res6,res7,res9,res10;
	if(avg_inter_div==0)
		res1=0;
	else
		res1=(avg_inter/avg_inter_div)/1000;
	if(avg_ser_div==0)
		res2=0;
	else
		res2=(avg_ser/avg_ser_div)/1000;
	if(res==0)
		res3=0;
	else
		res3=(avg_Q1_pkt/res);
	if(res==0)
		res4=0;
	else
		res4=(avg_Q2_pkt/res);
	if(res==0)
		res5=0;
	else
		res5=(avg_S1_pkt/res);
	if(res==0)
		res6=0;
	else
		res6=(avg_S2_pkt/res);
	if(avg_ser_div==0)
		res7=0;
	else
		res7=(avg_time_sys/avg_ser_div);
	if(totalT==0)
		res9=0;
	else
		res9=(double)tokendrop/totalT;
	if(num==0)
		res10=0;
	else
		res10=(double)packetdrop/avg_inter_div;
	printf("sum packet inter: %lf\n",avg_inter );
	printf("total packets: %d\n",pnum );
	fprintf(stdout, "    %s = %.8gs\n","average packet inter arrival time",res1 );
	fprintf(stdout, "    %s = %.8gs\n","average packet service time",res2 );
	fprintf(stdout, "\n    %s = %.8g\n","average number of packets in Q1",res3 );
	fprintf(stdout, "    %s = %.8g\n","average number of packets in Q2",res4 );
	fprintf(stdout, "    %s = %.8g\n","average number of packets in S1",res5 );
	fprintf(stdout, "    %s = %.8g\n","average number of packets in S2",res6 );
	fprintf(stdout, "\n    %s = %.8gs\n","average time a packet spent time in system", res7/1000);
	right_SD=((res7)*(res7));
	printf("left = %lf       ",left_SD/avg_ser_div);
	printf("%lf\n",right_SD);
	printf("server packets: %lf\n",avg_ser_div);
	fprintf(stdout, "    %s = %f\n","standard deviation of time spent in a system",(sqrt((left_SD/avg_ser_div) - ((right_SD) )) )/1000 );
	fprintf(stdout, "\n    %s = %.8g\n","token drop probablility",res9 );
	fprintf(stdout, "    %s = %.8g\n","packet drop probablility",res10 );
}


void handler(void * arg)
{
	int sig;
	sigwait(&set,&sig);
	pthread_mutex_lock(&mutex);
	totalPackets=0;
	pthread_mutex_unlock(&mutex);
	fprintf(stdout, "%s\n","SIGINT caught, no new packets or tokens will be allowed" );
	pthread_cancel(token_thread);
	pthread_cancel(packet_thread);
	pthread_join(packet_thread,0);
	pthread_join(token_thread,0);
	fprintf(stdout, "Q1List Length before unlinking : %d\n",My402ListLength(Q1List) );
	fprintf(stdout, "Q2List Length before unlinking : %d\n",My402ListLength(Q2List) );
	pthread_mutex_lock(&mutex);
	while(My402ListLength(Q1List)!=0)
	{
		My402ListElem* elem=My402ListFirst(Q1List);
		packetStr* p=(packetStr*)elem->obj;
		fprintf(stdout, "packet %d dropped from Q1\n",p->packet_num );
		My402ListUnlink(Q1List,elem);
	}
	while(My402ListLength(Q2List)!=0)
	{
		My402ListElem* elem=My402ListFirst(Q2List);
		packetStr* p=(packetStr*)elem->obj;
		fprintf(stdout, "packet %d dropped from Q2\n",p->packet_num );
		My402ListUnlink(Q2List,elem);
	}
	fprintf(stdout, "Q1List Length after unlinking : %d\n",My402ListLength(Q1List) );
	fprintf(stdout, "Q2List Length after unlinking : %d\n",My402ListLength(Q2List) );
	server=1;
	pthread_cond_broadcast(&cv);
	pthread_mutex_unlock(&mutex);
}

int main(int argc,char *argv[])
{
	if(argc%2==0)
		{
			fprintf(stderr, "%s\n","malformed input" );
			exit(1);
		}
	sigemptyset(&set);
	sigaddset(&set,SIGINT);
	pthread_sigmask(SIG_BLOCK,&set,0);
	pthread_create(&signal_thread,0,(void*)handler,0);

	struct timeval endt;
	Q1List=malloc(sizeof(My402List));
	Q2List=malloc(sizeof(My402List));
	token_bucket_list=malloc(sizeof(My402List));
	My402ListInit(Q1List);
	My402ListInit(Q2List);
	info *str=(info*)malloc(sizeof(info));
	str->lambda=1*1000;
	str->mu=(1/0.35)*1000;
	str->r=(1/1.5)*1000;
	str->B=10;
	str->P=3;
	str->num=20;
	process(str,argc,argv);
	if(str->fileCheck!=0L && str->fileCheck[0]!='\0')
		{
			mode=1;
			char line[1026];
			if(access(str->fileCheck, R_OK))
		    {
		      perror("Error");
		      exit(1);
		    }
			fp=fopen(str->fileCheck,"r");
			if(fp==0L)
			{
				perror("Error");
				exit(1);
			}
			if(fgets(line,1026,fp)!=0L)
			{
				pnum=atoi(line);
				str->num=atoi(line);
				if(strlen(line)==0)
				{
					fprintf(stderr,"%s\n","not a number in line 1");
					exit(1);
				}
				print(str);
			}
		}

	else
		{
			mode=0;
			num=str->num;
			pnum=num;
			totalPackets=num;
			print(str);
		}
	B=str->B;
	R=str->r;
	num=str->num;
	totalPackets=num;
	gettimeofday(&start,0L);
		
	fprintf(stdout, "%012.3lfms: %s\n",tconvert(start),"emulation begins" );
	serinfo* serone=malloc(sizeof(serinfo));
	serone->num=1;
	serone->busy=0;
	serinfo* sertwo=malloc(sizeof(serinfo));
	sertwo->num=2;
	sertwo->busy=0;
	pthread_create(&packet_thread,0,(void*)handle_packets,(void*)str);
	pthread_create(&token_thread,0,(void*)handle_tokens,0);
	pthread_create(&server_thread_1,0,(void*)handle_server,(void*)serone);
	pthread_create(&server_thread_2,0,(void*)handle_server,(void*)sertwo);
	pthread_join(packet_thread,0);
	printf("\npacket thread exited\n\n");
	pthread_join(token_thread,0);
	printf("\ntoken thread exited\n\n");
	pthread_join(server_thread_1,0);
	printf("\nserver 1 exited\n\n");
	pthread_join(server_thread_2,0);
	printf("\nserver 2 exited\n\n");
	//pthread_join(signal_thread,0);
	gettimeofday(&endt,0L);
	res=tconvert(endt);
	fprintf(stdout, "%012.3fms: %s\n",res,"emulation ends" );
	print_Stats();
	return 0;
}
